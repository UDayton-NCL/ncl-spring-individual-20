# What is the md5 hash of the password: "haseks354549"
echo -n haseks354549 | md5sum
# 6640173928eed60afb1fda8e7a1d601e  -

# What is the sha1 hash of the password: "61edel1993"
echo -n 61edel1993 | sha1sum
# 4efb3d169672db5fc189c1c1b86f24508d42f851  -

# What is the sha256 hash of the password: "71didmrpp"
echo -n 71didmrpp | sha256sum
# b1220ae7b63f195867213b38f711b2bfc18f3c759249b4b286a1d1e913c1158a  -
