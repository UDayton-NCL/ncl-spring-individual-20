# What time was the system rebooted? (round to the nearest minute)	
cat Windows\ Update.csv | grep -i reboot | cut -d',' -f2
# 1/19/2020 9:23:01 PM

# How many "Error" level events are logged?	
cat Windows\ Update.csv | cut -d',' -f1 | grep Error | wc -l
# 5


# How many times does the Microsoft-Windows-Kernel-Power Source record the system entering sleep?	
cat Windows\ Update.csv | grep Microsoft-Windows-Kernel-Power | grep -i "entering sleep" | wc -l
# 69

# When was the last Candy Crush Saga update installed? (round to the nearest minute)	
cat Windows\ Update.csv | grep "Installation Successful" | grep -i candy | head -n 1 | cut -d',' -f2
# 1/30/2020 11:50:13 AM


# How many security patches were installed on the machine?	
cat Windows\ Update.csv | grep Security | grep successfully | cut -d'(' -f2 | sort | uniq | wc -l
# 50