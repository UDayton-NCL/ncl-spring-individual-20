
# How many total sessions were recorded?	
cat authentication_log.json | wc -l
# 3255

# How many total bytes were transferred?	
cat transfer_log.json | cut -d',' -f4 | cut -d':' -f2 | cut -d'}' -f1 | awk '{s+=$1} END {printf "%.0f\n", s}'
# 93378598253

# On average, how many bytes are transferred in each row of the transfer log?
cat transfer_log.json | cut -d',' -f4 | cut -d':' -f2 | cut -d'}' -f1 | awk '{ total+=$1; count++ } END { printf "%.0f\n", total/count }'
# 5498357

# What was the maximum number of patches found by Windows Update in a single check?
cat Windows\ Update.csv | grep "found" | rev | cut -d' ' -f2 | rev | sort -n | tail -n 1
# 17