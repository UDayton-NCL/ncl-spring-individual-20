# How many total flow records are there?	
tail -n +2 aws_vpc_flow.log | wc -l
# 256677

# What date was this flow log captured? (in the local time of the log)	
date -d "@`head -n 2 aws_vpc_flow.log | tail -n 1 | cut -d' ' -f 11`"
# Fri 01 Nov 2019 05:21:32 PM PDT

# How many total bytes were transferred?
tail --lines=+2 aws_vpc_flow.log | cut -d' ' -f 10 | awk '{s+=$1} END {printf "%.0f\n", s}'
# 4402992854

# How many total packets were sent?	
tail --lines=+2 aws_vpc_flow.log | cut -d' ' -f 9 | awk '{s+=$1} END {printf "%.0f\n", s}'
# 19327871

# Which IP address had the most number of flow records?	
tail --lines=+2 aws_vpc_flow.log | cut -d' ' -f 4 | sort | uniq -c | sort -nr | head -n 1 | cut -d' ' -f 3
# 10.100.200.10

# How many bytes of UDP traffic was transferred?
tail --lines=+2 aws_vpc_flow.log | cut -d' ' -f8,10 | egrep "^17 " | cut -d' ' -f2 | awk '{s+=$1} END {print s}'
# 390944
