#!/usr/bin/env python3
from collections import defaultdict

bytes_sent_by_ip = defaultdict(int)
elapsed = []

with open('aws_vpc_flow.log') as f:
	lines = f.readlines()


for line in lines[1:]:
	if "SKIPDATA" in line or "NODATA" in line:
		continue
	line = line.split(' ')
	bytes_sent_by_ip[line[3]] += int(line[9])

	elapsed.append(int(line[-3]) - int(line[-4]))

print("What IP address sent the 5th most number of bytes?")
print(sorted(bytes_sent_by_ip.items(), key=lambda x: x[1])[-5][0])
# 47.198.69.107

print("What was the average duration of the flow records (in seconds)?")
print(round(sum(elapsed) / len(elapsed), 4))