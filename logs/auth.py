#!/usr/bin/env python3
import json
from datetime import datetime
from collections import defaultdict
from pprint import pprint
users = set()
user_sessions = dict()
auth_sessions = set()
tran_sessions = set()
session_bytes = defaultdict(int)
user_bytes    = defaultdict(int)
user_ips      = defaultdict(set)
session_ips   = defaultdict(set)
session_reqs  = defaultdict(int)
sessions_user = defaultdict(set)
session_start = dict()
session_times = defaultdict(list)

with open('authentication_log.json') as f:
	auth = json.load(f)

for entry in auth:
	users.add(entry['user'])
	auth_sessions.add(entry['sessionId'])
	user_sessions[entry['sessionId']] = entry['user']
	sessions_user[entry['user']].add(entry['sessionId'])
	session_start[entry['sessionId']] = datetime.strptime(entry['ts'], "%Y-%m-%dT%H:%M:%S.%fZ")

with open('transfer_log.json') as f:
	tran = json.load(f)

for entry in tran:
	tran_sessions.add(entry['sessionId'])
	session_bytes[entry['sessionId']] += entry['bytes']
	user_bytes[user_sessions[entry['sessionId']]] += entry['bytes']
	user_ips[user_sessions[entry['sessionId']]].add(entry['ip'])
	session_ips[entry['sessionId']].add(entry['ip'])
	session_reqs[entry['sessionId']] += 1
	session_times[entry['sessionId']].append(datetime.strptime(entry['ts'], "%Y-%m-%dT%H:%M:%S.%fZ"))


print("How many sessions were actually used in a transfer?")
print(len(auth_sessions & tran_sessions))

print("How many different users accessed the network?")
print(len(users))

print("On average, how many bytes were transferred in each active session?")
print(sum(session_bytes.values()) // len(session_bytes))

print("On average, how many total bytes did each user transfer across their sessions?")
print(sum(user_bytes.values()) // len(user_bytes))

# pprint(sorted(user_bytes.items(), key=lambda x: x[1]))

# all users use a single ip
for user in user_ips:
	if len(user_ips[user]) > 1:
		print(user)
# pprint(user_ips)


# all sessions only have 1 ip
for sess in session_ips:
	if len(session_ips[sess]) > 1:
		print(sess)
# pprint(session_ips)

# make sure at most 10 requests
for sess in session_reqs:
	if session_reqs[sess] > 10:
		print(sess)

"""
for sess in session_times:
	print(sess, session_start[sess])
	for time in session_times[sess]:
		if session_start[sess].day != session_times:
			print(sess, session_start[sess], time)
"""

# make sure session times are after the start
for user in sessions_user:
	active_sess = None
	for sess in sessions_user[user]:
		if len(session_times[sess]) == 0: continue
		start = session_start[sess]
		for time in session_times[sess]:
			if time < start:
				print("session_time is less that start")
				print(sess, start, time)

