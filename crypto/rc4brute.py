#!/usr/bin/env python3

from Crypto.Cipher import ARC4
import string

ALPHABET = string.ascii_uppercase
KEY_LENGTH = 4
ARC4.key_length = [KEY_LENGTH]


def check(key, data):
    decr = ARC4.new(key.encode()).decrypt(data)

    # horrible but use try catch......
    try: print('Key: {0}, Decryted Text: "{1}"'.format(key, decr.decode())) 
    except: return False
    return True

def worker():
    # 1834e1b2170c2ac5212677e3ae48ed42c32810400afca21defab111bc7
    data = b'\x18\x34\xe1\xb2\x17\x0c\x2a\xc5\x21\x26\x77\xe3\xae\x48\xed\x42\xc3\x28\x10\x40\x0a\xfc\xa2\x1d\xef\xab\x11\x1b\xc7'
    
    for a in ALPHABET:
        for b in ALPHABET:
            for c in ALPHABET:
                for d in ALPHABET:
                    if check(a+b+c+d, data): return

if __name__ == "__main__":
    worker()
